export const colors = {
    primary: '#35136C',
    black: '#000000',
    white: '#FFFFFF',
    secondaryGrey: '#D9D9D9',

    linkUnfocused: '#7152F2',
    linkFocused: '#45DD9D'
}
