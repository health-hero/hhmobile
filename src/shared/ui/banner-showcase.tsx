import {ReactNode, useEffect} from 'react'
import {Animated, ScrollView, StyleSheet} from 'react-native'

export type BannerShowcaseProps = {
    banners: BannerProps[]
}

export function BannerShowcase(props: BannerShowcaseProps) {
    return (
        <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={styles.showcase}>
            {
                props.banners.map((b) => Banner(b))
            }
        </ScrollView>
    )
}

type BannerProps = {
    content: ReactNode | null
}

function Banner(props: BannerProps) {
    const animated = new Animated.Value(0.2)

    const breathLoadingAnimation = Animated.loop(
        Animated.sequence([
            Animated.timing(animated, {
                toValue: 1,
                duration: 1500,
                useNativeDriver: true,
            }),
            Animated.timing(animated, {
                toValue: 0.2,
                duration: 1500,
                useNativeDriver: true,
            })
        ])
    )

    useEffect(() => {
        if (props.content) {
            breathLoadingAnimation.stop()
        } else {
            breathLoadingAnimation.start()
        }
    }, [props.content])

    return <Animated.View style={{
        ...styles.banner,
        opacity: animated,
    }} key={Math.random() * 1000}>
        { props.content }
    </Animated.View>
}

const styles = StyleSheet.create({
    banner: {
        width: 180,
        height: 100,
        borderRadius: 10,
        backgroundColor: '#C9C8D2',
        marginRight: 10,
    },
    showcase: {
        width: '100%',
        height: 100,
        maxHeight: 100,
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 27,
    }
})
