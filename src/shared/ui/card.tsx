import React from "react";
import {StyleSheet, View} from "react-native";
import {colors} from "./colors";

export function Card({ children }: React.PropsWithChildren): React.JSX.Element {
    return (
        <View style={styles.card}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 20,
        backgroundColor: colors.white,
        shadowColor: colors.black,
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 15,
        shadowOpacity: 1,
        elevation: 15,
        width: '100%',
        height: 'auto',
        marginBottom: 20,
    }
})
