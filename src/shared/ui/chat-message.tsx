import React from "react";
import {StyleSheet, Text, View} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {colors} from "./colors";

interface IChatMessage {
    isUserMessage: boolean
    text: string | null
    file: {
        previewImageSrc: string
        name: string
        size: number
    } | null
}

export function ChatMessage(props: IChatMessage): React.JSX.Element {
    return <View style={{
        ...styles.container,
        justifyContent: props.isUserMessage ? 'flex-end' : 'flex-start'
    }}>
        <LinearGradient
            style={styles.contentContainer}
            colors={props.isUserMessage ?
                ['#45DD9D', '#45D3DD']
                : ['#F6F4F8', '#F6F4F8']}>
            {
                props.file ?
                    <ChatMessageFile /> : null
            }

            {
                props.text && props.text.length ?
                    <Text style={{
                        ...styles.text,
                        color: props.isUserMessage ?
                            colors.white : colors.black
                    }}>
                        { props.text }
                    </Text> : null
            }
        </LinearGradient>
    </View>
}

function ChatMessageFile(): React.JSX.Element {
    return <View></View>
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
    },
    contentContainer: {
        borderRadius: 10,
        padding: 12,
        width: '80%',
    },
    text: {
        fontSize: 14,
        fontWeight: '400',
    }
})
