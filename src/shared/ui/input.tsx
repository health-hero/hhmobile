import React from 'react'
import {StyleSheet, Text, View} from "react-native";
import {colors} from "./colors";
import {KeyboardTypeOptions} from "react-native/Libraries/Components/TextInput/TextInput";
import {MaskedTextInput} from "react-native-mask-text";

type InputProps = {
    mask: string
    label: string
    placeholder: string
    keyboardType: KeyboardTypeOptions
    onChangeText: (text: string) => void
}

export function Input(props: InputProps): React.JSX.Element {
    return (
        <View style={styles.view}>
            <Text style={styles.text}>
                {props.label}
            </Text>

            <MaskedTextInput
                style={styles.textInput}
                placeholderTextColor={colors.secondaryGrey}
                mask={props.mask}
                keyboardType={props.keyboardType}
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        padding: 20,
    },
    text: {
        fontSize: 16,
        fontWeight: '400',
        color: colors.primary,
        marginBottom: 5,
    },
    textInput: {
        fontSize: 16,
        borderStyle: 'solid',
        borderColor: colors.primary,
        borderWidth: 1,
        borderRadius: 20,
        paddingHorizontal: 15,
        paddingVertical: 12,
        height: 45,
        width: '100%',
        fontWeight: '400',
        color: colors.primary
    },
})

