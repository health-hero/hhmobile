import {Image} from 'react-native'

export function TabBarHomeScreenIcon(props: { focused: boolean }) {
    return (
        <Image style={{ width: 30, height: 30 }}
               source={ props.focused ?
                        require('../../../assets/images/HomeScreenIconFocused.png')
                        : require('../../../assets/images/HomeScreenIcon.png') }
        />
    )
}
export function TabBarChatBotScreenIcon(props: { focused: boolean }) {
    return (
        <Image style={{ width: 30, height: 30 }}
               source={ props.focused ?
                   require('../../../assets/images/ChatBotScreenIconFocused.png')
                   : require('../../../assets/images/ChatBotScreenIcon.png') }
        />
    )
}
export function TabBarSettingsScreenIcon(props: { focused: boolean }) {
    return (
        <Image style={{ width: 30, height: 30 }}
               source={ props.focused ?
                   require('../../../assets/images/SettingsScreenIconFocused.png')
                   : require('../../../assets/images/SettingsScreenIcon.png') }
        />
    )
}


export function HomeNavHealthPassportIcon() {
    return <Image source={require('../../../assets/images/HealthPassport.png')} style={{ width: 25, height: 25 }}/>
}
export function HomeNavAnalyzesTranscriptionIcon() {
    return <Image source={require('../../../assets/images/AnalyzesTranscription.png')} style={{ width: 25, height: 25 }}/>
}
export function HomeNavSymptomCheckIcon() {
    return <Image source={require('../../../assets/images/SymptomCheck.png')} style={{ width: 25, height: 25 }}/>
}
export function HomeNavCheckUpSelectionPassportIcon() {
    return <Image source={require('../../../assets/images/CheckUpSelection.png')} style={{ width: 25, height: 25 }}/>
}
export function HomeNavClinicBookingIcon() {
    return <Image source={require('../../../assets/images/ClinicBooking.png')} style={{ width: 25, height: 25 }}/>
}

export function HomeNavArrowIcon() {
    return <Image source={require('../../../assets/images/HomeNavArrowIcon.png')} style={{ width: 15, height: 15 }}/>
}
