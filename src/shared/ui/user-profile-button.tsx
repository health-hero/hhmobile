import {Animated, Image, ImageSourcePropType, Pressable, StyleSheet, Text} from 'react-native'
import {colors} from 'shared/ui/colors'

type UserProfileButtonProps = {
    iconSource: ImageSourcePropType | null
    userName: string | null
    onPress: () => void
}

export function UserProfileButton(props: UserProfileButtonProps) {
    const animated = new Animated.Value(1)

    const fadeIn = () => { Animated.timing(animated, { toValue: 0.65, duration: 200, useNativeDriver: true }).start() }
    const fadeOut = () => { Animated.timing(animated, { toValue: 1, duration: 200, useNativeDriver: true }).start() }

    return <Animated.View style={{ width: '100%', opacity: animated }}>
        <Pressable onPress={props.onPress}
                   onPressIn={fadeIn}
                   onPressOut={fadeOut}
                   style={styles.container}>
            <Image style={styles.icon} source={
                props.iconSource ?? require('../../../assets/images/ProfileButtonIcon.png')
            } />
            <Text style={styles.userName}>
                { props.userName ?? 'Пользователь' }
            </Text>
        </Pressable>
    </Animated.View>
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 36,
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    icon: {
        width: 36,
        height: 36,
        marginRight: 10,
    },
    userName: {
        color: colors.black,
        fontSize: 20,
        lineHeight: 25,
        fontWeight: '400'
    }
})
