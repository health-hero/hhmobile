import React from "react";
import {Animated, Pressable, StyleSheet, Text} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {colors} from "./colors";

interface IButton {
    title: string
    onPress: () => void
}


export function PrimaryButton(props: IButton): React.JSX.Element {
    const animated = new Animated.Value(1)

    const fadeIn = () => {
        Animated.timing(animated, {
            toValue: 0.65,
            duration: 200,
            useNativeDriver: true,
        }).start()
    }
    const fadeOut = () => {
        Animated.timing(animated, {
            toValue: 1,
            duration: 200,
            useNativeDriver: true,
        }).start()
    }

    return (
        <Pressable onPress={props.onPress}
                   onPressIn={fadeIn}
                   onPressOut={fadeOut}
                   style={styles.pressable}>
            <Animated.View style={{ opacity: animated }}>
                <LinearGradient colors={['#5B2FFF', '#7655FE']}
                                style={styles.linearGradient}>
                    <Text style={styles.buttonText}>{props.title}</Text>
                </LinearGradient>
            </Animated.View>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    pressable: {
        borderRadius: 25,
        height: 55,
        shadowColor: 'rgba(125, 115, 231, 0.22)',
        shadowOffset: { width: 4, height: 4 },
    },
    linearGradient: {
        width: '100%',
        height: '100%',
        borderRadius: 25,
        paddingVertical: 12,
        paddingHorizontal: 24,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: colors.white,
        fontSize: 20,
        lineHeight: 20,
        fontWeight: '700'
    }
})
