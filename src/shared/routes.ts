import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs'
import {NativeStackNavigationProp} from '@react-navigation/native-stack'

export type RootTabNavigatorParamList = {
    Home: undefined
    ChatBot: undefined
    Settings: undefined
}

export type HomeStackNavigationParamList = {
    AnalyzesTranscription: undefined
    CheckUpSelection: undefined,
    ClinicBooking: undefined,
    HealthPassport: undefined,
    HomeIndex: undefined,
    SymptomCheck: undefined,
    UserProfile: undefined
}

export type RootTabNavigationProp = BottomTabNavigationProp<RootTabNavigatorParamList>

export type HomeStackNavigationProp = NativeStackNavigationProp<HomeStackNavigationParamList>
