import {StatusBar} from 'react-native'
import {NavigationContainer} from '@react-navigation/native'
import {
    BottomTabNavigationOptions,
    createBottomTabNavigator
} from '@react-navigation/bottom-tabs'
import {
    createNativeStackNavigator,
    NativeStackNavigationOptions
} from '@react-navigation/native-stack'

import { colors } from 'shared/ui/colors'
import {
    TabBarChatBotScreenIcon,
    TabBarHomeScreenIcon,
    TabBarSettingsScreenIcon
} from 'shared/ui/icons'
import {
    HomeStackNavigationParamList,
    RootTabNavigatorParamList
} from 'shared/routes'

import {AnalyzesTranscriptionScreen} from 'pages/analyzes-transcription'
import {ChatBotScreen} from 'pages/chat-bot'
import {CheckUpSelectionScreen} from 'pages/check-up-selection'
import {ClinicBookingScreen} from 'pages/clinic-booking'
import {HealthPassportScreen} from 'pages/health-passport'
import {HomeScreen} from 'pages/home'
import {SettingsScreen} from 'pages/settings'
import {SymptomCheckScreen} from 'pages/symptom-check'
import {UserProfileScreen} from 'pages/user-profile'

const Tab = createBottomTabNavigator<RootTabNavigatorParamList>()

const Stack = createNativeStackNavigator<HomeStackNavigationParamList>()

export function Routing() {
    return <NavigationContainer>
        <StatusBar barStyle={'dark-content'} backgroundColor={colors.white} />
        <Tab.Navigator screenOptions={tabNavigatorOptions}>
            <Tab.Screen name={'Home'} component={HomeStackNavigator} options={{
                title: 'Главная',
                tabBarIcon: ({ focused }) => <TabBarHomeScreenIcon focused={focused} />
            }}  />
            <Tab.Screen name={'ChatBot'} component={ChatBotScreen} options={{
                title: 'Чат-бот',
                tabBarIcon: ({ focused }) => <TabBarChatBotScreenIcon focused={focused} />
            }} />
            <Tab.Screen name={'Settings'} component={SettingsScreen} options={{
                title: 'Настройки',
                tabBarIcon: ({ focused }) => <TabBarSettingsScreenIcon focused={focused} />
            }} />
        </Tab.Navigator>
    </NavigationContainer>
}

function HomeStackNavigator() {
    return <Stack.Navigator screenOptions={navigatorOptions} initialRouteName={'HomeIndex'}>
        <Stack.Screen name={'HomeIndex'} component={HomeScreen} options={{
            headerShown: false
        }} />
        <Stack.Screen name={'HealthPassport'} component={HealthPassportScreen} options={{
            title: 'Паспорт здоровья'
        }} />
        <Stack.Screen name={'AnalyzesTranscription'} component={AnalyzesTranscriptionScreen} options={{
            title: 'Расшифровка анализов'
        }} />
        <Stack.Screen name={'SymptomCheck'} component={SymptomCheckScreen} options={{
            title: 'Проверка симптомов'
        }} />
        <Stack.Screen name={'CheckUpSelection'} component={CheckUpSelectionScreen} options={{
            title: 'Подбор чек-апа'
        }} />
        <Stack.Screen name={'ClinicBooking'} component={ClinicBookingScreen} options={{
            title: 'Запись в клинику'
        }} />
        <Stack.Screen name={'UserProfile'} component={UserProfileScreen} options={{
            title: 'Профиль'
        }} />
    </Stack.Navigator>
}

const tabNavigatorOptions: BottomTabNavigationOptions = {
    tabBarStyle: {
        paddingTop: 7,
        paddingBottom: 9,
        height: 65,
        shadowColor: colors.black,
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 20,
        shadowOpacity: 1,
        elevation: 20,
    },
    tabBarActiveTintColor: colors.linkFocused,
    tabBarInactiveTintColor: colors.linkUnfocused,
    tabBarLabelStyle: { fontSize: 14, fontWeight: '400', lineHeight: 18 },
    tabBarHideOnKeyboard: true,
    headerShown: false,
}

const navigatorOptions: NativeStackNavigationOptions = {
    headerTitleStyle: {
        fontWeight: '500',
        color: colors.primary
    },
}
