import {ScrollView, StyleSheet, Text, View} from 'react-native'
import {colors} from 'shared/ui/colors'
import {Card} from 'shared/ui/card'

export function HealthPassportScreen() {
    return <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
            <Card>
                <View style={styles.healthScoringCard}>
                    <View style={styles.healthScore}>
                        <Text style={styles.healthScoreText}>
                            9,2 из 10
                        </Text>
                    </View>
                    <Text style={styles.healthScoringCardHeading}>
                        Ваша оценка здоровья
                    </Text>
                    <Text style={styles.healthScoringCardText}>
                        Так держать!
                    </Text>
                </View>
            </Card>
        </View>
    </ScrollView>
}

const styles = StyleSheet.create({
    scrollView: {
        width: '100%',
        minHeight: '100%',
        backgroundColor: colors.white,
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        paddingTop: 15,
        paddingHorizontal: 20,
    },
    healthScoringCard: {
        padding: 12,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    healthScore: {
        width: 130,
        height: 130,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        /* TODO: is temporary, fix it */
        borderColor: colors.linkFocused,
        borderRadius: 130,
        borderWidth: 10
    },
    healthScoreText: {
        fontSize: 18,
        fontWeight: '700',
        lineHeight: 28,
        color: colors.linkFocused
    },
    healthScoringCardHeading: {
        fontSize: 16,
        fontWeight: '400',
        color: colors.primary
    },
    healthScoringCardText: {
        fontSize: 14,
        fontWeight: '400',
        color: colors.primary
    }
})
