import React from "react";
import {useNavigation} from '@react-navigation/native'
import {Animated, Pressable, SafeAreaView, StyleSheet, Text, View} from 'react-native'

import { Card } from 'shared/ui/card'
import { colors } from 'shared/ui/colors'
import {
    HomeNavAnalyzesTranscriptionIcon, HomeNavArrowIcon,
    HomeNavCheckUpSelectionPassportIcon, HomeNavClinicBookingIcon,
    HomeNavHealthPassportIcon, HomeNavSymptomCheckIcon,
} from 'shared/ui/icons'
import {BannerShowcase} from 'shared/ui/banner-showcase'
import {HomeStackNavigationProp} from 'shared/routes'
import {UserProfileButton} from 'shared/ui/user-profile-button'

export function HomeScreen() {
    const navigation = useNavigation<HomeStackNavigationProp>()

    const buttons: IHomeNavButton[] = [
        {
            icon: <HomeNavHealthPassportIcon />,
            label: 'Паспорт здоровья',
            onPress: () => navigation.navigate('HealthPassport')
        },
        {
            icon: <HomeNavAnalyzesTranscriptionIcon />,
            label: 'Расшифровка анализов',
            onPress: () => navigation.navigate('AnalyzesTranscription')
        },
        {
            icon: <HomeNavSymptomCheckIcon />,
            label: 'Проверка симптомов',
            onPress: () => navigation.navigate('SymptomCheck')
        },
        {
            icon: <HomeNavCheckUpSelectionPassportIcon />,
            label: 'Подбор чек-апа',
            onPress: () => navigation.navigate('CheckUpSelection')
        },
        {
            icon: <HomeNavClinicBookingIcon />,
            label: 'Запись в клинику',
            onPress: () => navigation.navigate('ClinicBooking')
        },
    ]

    return <SafeAreaView>
        <View style={styles.container}>
            <UserProfileButton iconSource={null} userName={null}
                               onPress={() => navigation.navigate('UserProfile') } />

            <BannerShowcase banners={[
                { content: null },
                { content: null },
                { content: null },
            ]} />

            <Card>
                {
                    buttons.map(
                        (b, index) =>
                            <HomeNavButton key={index}
                                           onPress={b.onPress}
                                           label={b.label}
                                           icon={b.icon} />
                    )
                }
            </Card>
        </View>
    </SafeAreaView>
}

interface IHomeNavButton {
    icon: React.JSX.Element,
    label: string,
    onPress: () => void
}

function HomeNavButton(props: IHomeNavButton): React.JSX.Element {
    const animated = new Animated.Value(1)

    const fadeIn = () => { Animated.timing(animated, { toValue: 0.5, duration: 200, useNativeDriver: true }).start() }
    const fadeOut = () => { Animated.timing(animated, { toValue: 1, duration: 200, useNativeDriver: true }).start() }

    return <Animated.View style={{ ...styles.homeNavButton, opacity: animated }}>
        <Pressable
            onPress={props.onPress}
            onPressIn={fadeIn}
            onPressOut={fadeOut}
            style={styles.homeNavButtonPressable}
        >
            <View style={styles.homeNavButtonLabel}>
                <View style={{ marginRight: 14 }}>
                    { props.icon }
                </View>
                <Text style={styles.homeNavButtonLabelText}>
                    { props.label }
                </Text>
            </View>
            <HomeNavArrowIcon />
        </Pressable>
    </Animated.View>
}

const styles = StyleSheet.create({
    scrollView: {
        width: '100%',
        height: '100%',
    },
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: colors.white,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 20,
    },
    homeNavButton: {
        width: '100%',
        height: 60
    },
    homeNavButtonPressable: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: colors.secondaryGrey
    },
    homeNavButtonLabel: {
        display: 'flex',
        flexDirection: 'row',
    },
    homeNavButtonLabelText: {
        fontSize: 18,
        fontWeight: '500',
        lineHeight: 25,
        color: '#5B2FFF',
    },
})
