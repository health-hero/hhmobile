import {useState} from 'react'
import {Alert, SafeAreaView, StatusBar, Text, View} from 'react-native'
import {useNavigation} from '@react-navigation/native'
import {Input} from 'shared/ui/input'
import {colors} from 'shared/ui/colors'
import {PrimaryButton} from 'shared/ui/buttons'
import {RootTabNavigationProp} from 'shared/routes'

export function Login() {
    const navigation = useNavigation<RootTabNavigationProp>()

    const [phone, setPhone] = useState<string>('')
    const [waitCode, setWaitCode] = useState<boolean>(false)

    return <SafeAreaView>
        <StatusBar barStyle={'dark-content'} backgroundColor={colors.white} />
        <View style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '100%',
            backgroundColor: colors.white,
        }}>
            <Text style={{
                textAlign: 'center',
                fontSize: 40,
                fontWeight: '500',
                lineHeight: 50,
                color: colors.primary,
            }}>Health Hero</Text>

            <Text style={{
                textAlign: 'center',
                fontSize: 32,
                fontWeight: '400',
                lineHeight: 40,
                color: colors.primary,
                marginBottom: 30
            }}>Добро пожаловать!</Text>

            {
                waitCode ?
                    <>
                        <Input label={'Введите код из СМС'}
                               placeholder={'000 000'}
                               mask={'999 999'}
                               keyboardType={'number-pad'}
                               onChangeText={() => {}}
                        />
                        <PrimaryButton
                            title={'Войти'}
                            onPress={() => {
                                Alert.alert('Дальше надо сделать', 'навигацию по экранам')
                            }} />
                    </>
                    :
                    <>
                        <Input label={'Введите номер телефона'}
                               placeholder={'+7 777 777 77 77'}
                               mask={'+7 999 999-99-99'}
                               keyboardType={'number-pad'}
                               onChangeText={(phone) => setPhone(phone)}
                        />
                        <PrimaryButton
                            title={'Получить код'}
                            onPress={() => {
                                if (phone.length === 16) {
                                    /* TODO: send request */
                                    setWaitCode(true)
                                }
                            }} />
                    </>
            }
        </View>
    </SafeAreaView>
}

