import {useEffect, useRef} from "react";
import {SafeAreaView, ScrollView, StyleSheet, TextInput, View} from "react-native";
import {ChatMessage, colors} from "../../shared";

const text = 'Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha Wazzup brotha'

export function ChatBotScreen() {
    const scrollViewRef = useRef<ScrollView>(null)

    useEffect(() => {
        if (scrollViewRef.current) {
            scrollViewRef.current.scrollToEnd({ animated: true })
        }
    }, [])

    return (
        <SafeAreaView>
            <ScrollView style={styles.scrollView} ref={scrollViewRef}>
                <View style={styles.container}>
                    <ChatMessage isUserMessage={true} text={text} file={null} />
                    <ChatMessage isUserMessage={false} text={text} file={null} />
                    <ChatMessage isUserMessage={true} text={text} file={null} />
                    <ChatMessage isUserMessage={false} text={text} file={null} />
                    <ChatMessage isUserMessage={true} text={text} file={null} />
                    <ChatMessage isUserMessage={false} text={text} file={null} />
                    <ChatMessage isUserMessage={true} text={text} file={null} />
                    <ChatMessage isUserMessage={false} text={text} file={null} />
                    <ChatMessage isUserMessage={true} text={text} file={null} />
                    <ChatMessage isUserMessage={false} text={text} file={null} />
                    <ChatMessage isUserMessage={true} text={'Это последнее сообщение, Это последнее сообщение'} file={null} />
                </View>
                <View style={styles.inputContainer}>
                    <ScrollView style={styles.inputSuggestions}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}>

                    </ScrollView>
                    <TextInput
                        placeholder={'Сообщение'}
                        style={chatInputStyles.input}
                        multiline={true}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    scrollView: {
        width: '100%',
        minHeight: '100%',
        backgroundColor: colors.white,
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        paddingTop: 15,
        paddingHorizontal: 20,
    },
    inputContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    inputSuggestions: {

    },
})

const chatInputStyles = StyleSheet.create({
    input: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: colors.white,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        shadowColor: colors.black,
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 15,
        shadowOpacity: 1,
        elevation: 15,
    }
})
