/**
 * @format
 */
import {AppRegistry} from 'react-native'
import {name as appName} from './app.json'
import Bootstrap from './App'

AppRegistry.registerComponent(appName, () => Bootstrap);
